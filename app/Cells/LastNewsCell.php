<?php

use App\Models\Content;

class LastNewsCell
{
    public static function show()
    {

        $news = Content::live()
            ->byCategoryId('news')
            ->visible()
            ->orderBy('publish_at', 'desc')
            ->get();


        return view('cells.news.last', [
            'news' => $news
        ]);
    }
}

