<?php


use App\Lib\Models\Traits\Singleton;
use App\Lib\YamlTree\Tree;


class AdminMenuCell extends Tree
{
    protected static $instance = null;

    const LOCALE_SCOPE = null;
    const STRUCT_PATH  = 'static_data/admin/menu_struct.yaml';
//
//    private static function get_file_path() {
//        return database_path(static::STRUCT_PATH);
//    }



    public static function show() {
        $root_node = static::root();

        return view('admin.cells.menu.show', [
            'root_node' => $root_node
        ]);
    }

    public static function menu_item($node)
    {

        return view('admin.cells.menu.item', [
            'node'  => $node,
            'url'   => static::node_url($node),
            'title' => static::node_title($node)
        ]);

    }

    private static function node_url($node)
    {
//        print_r(route( $node->url['route'], $node->url['params'] ));
//        exit;
        $url = isset( $node->url['route'] ) ? route( $node->url['route'], $node->url['params'] ) :
            (isset( $node->url['link'] ) ? $node->url['link'] : null);

        return $url;
    }


    private static function node_title($node)
    {
        $title = isset( $node->title ) ? $node->title : null;
        if ( isset( $node->title_method ) ) {
            $method = $node->title_method;
            $title  = static::$method();
        }

        return $title;
    }

    private static function current_user_name() {
        return Auth::user()->name;
    }
}

