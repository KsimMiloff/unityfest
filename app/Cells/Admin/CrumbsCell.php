<?php


class AdminCrumbs
{

    public function __construct(array $attributes = []) {
        $this->crumbs = [];
    }


    public static function items($attributes) {
        return (new static($attributes))->crumbs;
    }


    public static function show($attributes = []) {

        $crumbs = static::items($attributes);

        if ( count($crumbs) )
        {
            return view('admin.cells.crumbs.show', [
                'crumbs' => $crumbs
            ]);
        }
    }

}

class AdminUserCrumbs extends AdminCrumbs
{
    public function __construct($attributes) {

        parent::__construct($attributes);

        $user = $attributes['user'];
        $t_chain = $user->exists ? 'crud.edit.user' : 'crud.new.user';

        $crumbs = [
            [
                'title' => trans( 'crud.list.user' ),
                'url' => route('admin.users.index')
            ],
            [
                'title' => trans( $t_chain, ['title' => $user->email] ),
                'active' => true
            ]
        ];
        $this->crumbs = collect($crumbs);
    }
}

class AdminContentCrumbs extends AdminCrumbs
{
    public function __construct($attributes) {

        parent::__construct($attributes);

        $content = $attributes['content'];
        $t_chain = $content->exists ? 'crud.edit.content' : 'crud.new.content';

        $crumbs = [
//            [
//                'title' => trans( 'contents.model' ),
//                'url' => route('admin.contents.index', ['category_id' => $content->category_id])
//            ],
            [
                'title' => "&laquo;{$content->category->title()}&raquo;",
                'url' => route('admin.contents.index', ['category_id' => $content->category_id])
            ],
            [
                'title' => trans( $t_chain, ['title' => $content->title] ),
                'active' => true
            ]
        ];
        $this->crumbs = collect($crumbs);


    }
}

