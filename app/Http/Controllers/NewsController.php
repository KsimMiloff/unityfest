<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;

use App\Models\Content;
use App\Lib\XFields\Category\ContentCategory;
//use Mockery\CountValidator\Exception;


class NewsController extends Controller
{

    public function index(Request $request)
    {
        $category = ContentCategory::find('news');

        try {
            $years = [
                'start' => Content::live()->byCategoryId($category->id)->visible()->ultraDate('max', 'publish_at')->year,
                'end'   => Content::live()->byCategoryId($category->id)->visible()->ultraDate('min', 'publish_at')->year,
            ];
        } catch( \Exception $e ){
            $years = ['start' => date('Y'), 'end' => date('Y') ];
        }

        $current_year = $request->year ? $request->year : $years['end'];

        $news = Content::live()->visible()
            ->byCategoryId($category->id)
            ->whereYear('publish_at', '=', $current_year)
            ->orderBy('publish_at', 'desc')
            ->get();

        return view('news.index', [
            'news' => $news,
            'category' => $category,
            'years' => $years,
            'current_year' => $current_year
        ]);
    }

    public function show(Request $request, $id)
    {
        $content = Content::live()->visible()
            ->bySlug($id)->first();

        return view('news.show', [
            'content' => $content,
        ]);
    }

}
