<?php

namespace App\Http\Controllers;
use App\Http\Controllers\Controller;

use Illuminate\Http\Request;

use App\Http\Requests;

use App\Models\SinglePage;
use App\Models\Content;
use App\Lib\Nii\Tree as NiiTree;


class WelcomeController extends Controller
{
    public function index()
    {
        $page = SinglePage::where( 'category_id', 'about' )->first();


        return view('welcome.show', [
            'page' => $page,
        ]);
    }



}
