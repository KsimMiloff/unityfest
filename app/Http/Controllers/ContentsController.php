<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;

use App\Models\Content;
use App\Lib\XFields\Category\ContentCategory;


class ContentsController extends Controller
{


    public function show(Request $request, $id)
    {
        $content = Content::live()->visible()
            ->bySlug($id)->first();

        return view('contents.show', [
            'content' => $content,
        ]);
    }

}
