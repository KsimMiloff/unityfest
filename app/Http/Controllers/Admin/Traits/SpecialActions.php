<?php

namespace App\Http\Controllers\Admin\Traits;

trait SpecialActions
{

    private function run_action( $object, $options=[] )
    {
        $object_state = $object->exists ? 'update' : 'create';

        if ( request()->has('action.save') )
        {
            $result = $this->save($object);
        }

        if ( request()->has('action.save_and_new') )
        {
            $result = $this->save_and_new($object);
        }

        if ( request()->has('action.move_to_archive') && $object->move_to_archive() )
        {
            flash()->success(trans('crud.move_to_archive.success'));
            $result =  redirect()->back();
        }

        if ( request()->has('action.move_to_live') && $object->move_to_live())
        {
            flash()->success( trans( 'crud.move_to_live.success' ) );
            $result =  redirect()->back();
        }

        if ($result) {
            return $result;
        }


        flash()->error( trans( "crud.$object_state.error" ) );
        return view("admin." . static::VIEWS_FOLDER . ".form", [static::VIEW_VAR_NAME => $object]);

    }

    private function save( $object )
    {

        if ( $object->save() )
        {
            flash()->success( trans( 'crud.update.success' ) );
            return redirect()->route( 'admin.' . static::VIEWS_FOLDER . '.edit', ['id' => $object] );

        }

        return false;


    }

    private function save_and_new( $object )
    {
        if ( $object->save() )
        {
            flash()->success( trans( 'crud.update.success' ) );
            return redirect()->route( 'admin.' . static::VIEWS_FOLDER . '.create', ['category_id' => $object->category_id] );

        }

        return false;

    }

}