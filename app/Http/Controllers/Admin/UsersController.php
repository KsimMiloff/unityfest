<?php

namespace App\Http\Controllers\Admin;

use Gate;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use App\User;


class UsersController extends Controller
{
    public function index()
    {
        $users = User::all();


        return view('admin.users.index', [
            'users' => $users,
        ]);
    }

    public function create()
    {
        $user = new User();

        return view('admin.users.form', ['user' => $user]);
    }


    public function store(Request $request)
    {
        $user = new User($this->user_params($request));
        $user->email = $request->get('email');

        if ($request->has('password')) {
            $user->set_password($request->get('password'));
        }

        if (Gate::allows('toggle-admin-state')) { // давать/забирать права админа может только другой админ
            $user->is_admin = $request->get('is_admin');
        }

        if ( $user->save() ) {
            flash()->success('Пользователь создан');

            return redirect()->route('admin.users.index');

        } else {
            flash()->error('Пользователь не был создан!');
            return view('admin.users.form', ['user'   => $user]);
        }
    }


    public function edit(Request $request, $id)
    {

        $user = User::find($request->route('users'));

        return view('admin.users.form', ['user' => $user]);
    }


    public function update(Request $request, $id)
    {
        $user = User::find($request->route('users'));

        if ( Gate::allows('update-user', $user) )
        {

            $user->fill($this->user_params($request));

            if ($request->has('password')) {
                $user->set_password($request->get('password'));
            }

            if (Gate::allows('toggle-admin-state')) { // давать/забирать права админа может только другой админ
                $user->is_admin = $request->get('is_admin');
            }

            if ( $user->save() ) {
                flash()->success('Пользователь сохранен');
            } else {
                flash()->error('Сохранить пользователя не удалось');

                return view('admin.users.form', ['user'    => $user]);
            }

        } else {
            flash()->error('Нет прав для изменения этого пользователя');
        }

        return redirect()->route("admin.users.index",['id' => $user]);
    }

    public function destroy($id)
    {
        //
    }

    private function user_params($request)
    {
        return $request->user ?: [];
    }

    private function crumbs($user)
    {
        $crumbs = [
            ['title' => 'Список пользователей', 'url' => route('admin.users.index')],
        ];

        if ( $user->exists ) {
            $crumb = ['title' => "Редактривание пользователя {$user->email}", 'active' => true];
        } else {
            $crumb = ['title' => "Добавление нового пользователя", 'active' => true];
        }

        array_push($crumbs, $crumb);

        return collect($crumbs);
    }
}
