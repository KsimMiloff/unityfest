<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use App\Models\Content;
use App\Lib\XFields\Category\ContentCategory;

use App\Http\Controllers\Admin\Traits\SpecialActions;

class ContentsController extends Controller
{
    use SpecialActions;

    const VIEWS_FOLDER = 'contents';
    const VIEW_VAR_NAME = 'content';

    public function index(Request $request)
    {
        $state       = $request->get('state', 'live');
        $category_id = $request->category_id; // x$category_id = $request->session()->get('content.category_id');

        $category = ContentCategory::find($category_id);
        $contents = Content::byState($state);

        if ($category_id) {
            $contents = $contents->byCategoryId($category_id);
        }

        $contents = $contents->fresh()->get();


        return view('admin.contents.index', [
            'contents'    => $contents,
            'categories'  => ContentCategory::all(),
            'category' => $category,
        ]);
    }

    public function create(Request $request, $category_id)
    {
        $content = new Content(['category_id' => $category_id]);
        return view('admin.contents.form', ['content' => $content]);
    }


    public function store(Request $request)
    {
        $content = new content($this->content_params());

        return $this->run_action( $content );
    }


    public function show($id)
    {
        //
    }


    public function edit(Request $request, $id)
    {
        $content = Content::find($id);
        return view('admin.contents.form', ['content' => $content]);
    }


    public function update(Request $request, $id)
    {

        $content = Content::find($id)
            ->fill($this->content_params());

        return $this->run_action( $content );
    }



    private function content_params()
    {
        return request()->content;
    }


}
