<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use App\Models\SinglePage;

class SinglePagesController extends Controller
{

    public function store(Request $request)
    {
        $page = new SinglePage($this->page_params());

        if ( $page->save() ) {
            flash()->success( trans( 'crud.store.success' ) );
            return redirect()->route( 'admin.single_pages', ['category_id' => $page->category_id]);

        }
        flash()->error( trans( 'crud.store.error' ) );
        return view('admin.single_pages.form', ['page' => $page]);

    }

    public function edit(Request $request, $category_id)
    {
        $page = SinglePage::where(['category_id' => $category_id])->first();
        if (! $page)
        {
            $page = new SinglePage([
                'category_id' => $category_id,
            ]);

        }

        return view('admin.single_pages.form', ['page' => $page,]);
    }


    public function update(Request $request, $id)
    {
        $page = SinglePage::find($id)
            ->fill($this->page_params());


        if ( $page->save() )
        {
            flash()->success( trans( 'crud.update.success' ) );
            return redirect()->route( 'admin.single_pages', ['category_id' => $page->category_id] );
        }

        flash()->error( trans( 'crud.update.error' ) );
        return view('admin.single_pages.form', ['page' => $page]);
    }


    private function page_params()
    {
        $params  = request()->single_page ?: [];
        return $params;
    }

}
