<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;

use App\Models\Content;
use App\Lib\XFields\Category\ContentCategory;


class GalleriesController extends Controller
{

    public function index(Request $request)
    {
        $gallery = Content::live()->visible()
            ->byCategoryId('gallery')
            ->fresh()
            ->first();

        return $this->show($request, $gallery->id);
    }

    public function show(Request $request, $id)
    {
        $category = ContentCategory::find('gallery');

        $gallery = Content::live()->visible()
            ->byCategoryId('gallery')
            ->bySlug($id)->first();

        return view('galleries.show', [
            'gallery' => $gallery,
            'category' => $category,
        ]);
    }

}
