<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;

use App\Models\SinglePage;
use App\Lib\XFields\Category\SinglePageCategory;


class SinglePagesController extends Controller
{


    public function show(Request $request, $category_id)
    {
        $page = SinglePage::where( 'category_id', $category_id )->first();

        return view($this->show_view($category_id), [
            'page' => $page,
        ]);
    }


    private function show_view($category_id) {
        $default_view = 'single_pages.show';
        $view = "$default_view.$category_id";

        if (\View::exists($view)) {
            return $view;
        }

        return $default_view;
    }

}
