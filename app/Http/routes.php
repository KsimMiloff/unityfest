<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/


Route::get('/login', ['as' => 'auth.login', 'uses' => 'Auth\AuthController@getLogin']);
Route::post('/login', ['as' => 'auth.login', 'uses' => 'Auth\AuthController@postLogin']);
Route::get('/logout', ['as' => 'auth.logout', 'uses' => 'Auth\AuthController@logout']);

Route::group(['namespace' => 'File'], function()
{
    Route::post('files/wisiwyg_upload}',
        ['as' => 'files.wisiwyg_upload', 'uses' => 'FilesController@wisiwyg_upload']
    );

    Route::get('images/{id}/resized/{size}/{crop?}',
        ['as' => 'images.resized', 'uses' => 'ImagesController@resize']
    );

    Route::resource('images', 'ImagesController', ['except' => ['destroy']]);
    Route::resource('files', 'FilesController', ['except' => ['destroy']]);
});


Route::get('admin', function () {
    return redirect('/admin/singe_pages/about');
});


Route::group(['namespace' => 'Admin', 'prefix' => 'admin', 'middleware' => 'auth'], function()
{

    Route::resource('users', 'UsersController', ['except' => ['show', 'destroy']]);

    Route::get('contents/create/{category_id}', ['as' => 'admin.contents.create', 'uses' => 'ContentsController@create']);
    Route::get('contents/{category_id}', ['as' => 'admin.contents.index', 'uses' => 'ContentsController@index']);
    Route::resource('contents', 'ContentsController', ['except' => ['index', 'create']]);

    Route::get('singe_pages/{category_id}', [
        'as' => 'admin.single_pages',
        'uses' => 'SinglePagesController@edit'
    ])->where([
        'category_id'  => 'about|cooperation|contacts|programm'
    ]);
    Route::resource('single_pages', 'SinglePagesController', ['only' => ['store', 'update']]);


});


Route::get('/', 'WelcomeController@index');

Route::get('{single_pages_category}',
    ['as' => 'single_pages.show', 'uses' => 'SinglePagesController@show']
)->where([
    'single_pages_category' => 'about|cooperation|contacts|programm'
]);

Route::resource('contents', 'ContentsController', ['only' => ['show']]);
Route::resource('news', 'NewsController', ['only' => ['index', 'show']]);
Route::resource('galleries', 'GalleriesController', ['only' => ['index', 'show']]);
