<?php

namespace App\Lib\XFields\Field;

use Form;

class UrlField extends BaseField
{

    public function from_db_format($value) {
        return $value;
    }

    public function to_db_format($value) {
        return $value;
    }

    public function field($field_alias, $value=null) {
        return Form::url($field_alias, $value, ['class' => "form-control"] );
    }

    public function widget($options, $class='col-sm-5') {
        return parent::widget($options, $class);
    }

}
