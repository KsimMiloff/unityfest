<?php

namespace App\Lib\XFields\Field;

use Form;

class DateField extends BaseField
{
    public function from_db_format($value) {
//        if ($value) {
//
//            exit;
//        }
//        return $value->format('m/d/Y');
        return \Carbon\Carbon::parse($value)->format('d.m.Y');
    }


    public function to_db_format($value)
    {
        return \Carbon\Carbon::parse($value);
    }

    public function field($field_name, $value=null) {
        $selector = 'date_picker_field';
        $class = implode( ' ', ['form-control', $selector] );

        $field  = Form::text($field_name, $value, ['class' => $class]);
        $script = $this->script($selector);

        return "$field $script";
    }

    private function script($selector) {
        return "
            <script>
                $('.$selector').datepicker({
                    language: 'ru',
                    format: 'dd.mm.yyyy',
                    autoclose: true,
                    todayHighlight: true
                })
            </script>
        ";
    }

}
