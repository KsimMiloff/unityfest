<?php

namespace App\Lib\XFields\Field;

use Form;

class TextField extends BaseField
{

    public function from_db_format($value) {
        return $value;
    }

    public function to_db_format($value)
    {
        return $value;
    }

    public function field($field_alias, $value=null) {
        return Form::textarea($field_alias, $value, ['class' => "form-control", 'rows' => 5] );
    }

    public function widget($options, $class='col-sm-8') {
        return parent::widget($options, $class);
    }

}
