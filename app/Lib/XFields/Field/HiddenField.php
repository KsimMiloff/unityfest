<?php

namespace App\Lib\XFields\Field;

use Form;

class HiddenField extends TextField
{

    public function field($field_alias, $value=null) {
        return Form::hidden($field_alias, $value);
    }


    public function html($label, $field, $class)
    {
        return $field;
    }

}
