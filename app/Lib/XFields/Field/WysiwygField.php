<?php

namespace App\Lib\XFields\Field;

use Form;

class WysiwygField extends TextField
{

    public function field($field_alias, $value=null) {
        return Form::wisiwyg($field_alias, $value, ['class' => "form-control"] );
    }

}
