<?php

namespace App\Lib\XFields\Field;

use Form;

class IntegerField extends BaseField
{

    public function from_db_format($value) {
        return (int) $value;
    }

    public function to_db_format($value) {
        return (int) $value;
    }

    public function field($field_alias, $value=null) {
        return Form::number($field_alias, $value, ['class' => "form-control", 'step' => 1] );
    }


}
