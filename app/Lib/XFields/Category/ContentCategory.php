<?php

namespace App\Lib\XFields\Category;

use App\Lib\XFields\Field\BaseField;
use App\Lib\XFields\Category\SeoableCategory;

class ContentCategory extends SeoableCategory
{

    public static function hash() {

        return [
            'news' => 'contents.news',
            'gallery' => 'contents.gallery',
            'about' => 'contents.about',
            'programm' => 'contents.programm',
        ];
    }


    public static function category_fields()
    {
        $fields = [];
        foreach (static::hash() as $key => $val)
        {
            $method = $key . "_fields";
            $fields[$key] = static::$method();
        }

        return $fields;
    }

    private static function news_fields() {
        return [

            'announce' => BaseField::create([
                'title' => 'Кратко',
                'storage' => 'props.announce',
                'type' => 'Text'
            ]),

            'desc' => BaseField::create([
                'title' => 'Подробно',
                'storage' => 'desc',
                'type' => 'Wysiwyg'
            ]),

//            'image_id' => BaseField::create([
//                'title'   => 'Картинка',
//                'storage' => 'props.image_id',
//                'type'    => 'Image',
//            ]),

            'publish_at' => BaseField::create([
                'title'   => 'Дата публикации',
                'storage' => 'publish_at',
                'type'    => 'Date',
            ]),

        ];
    }


    private static function gallery_fields() {
        return [

            'image_ids' => BaseField::create([
                'title' => 'Фотографии',
                'storage' => 'props.image_ids',
                'type' => 'Album'
            ]),

            'publish_at' => BaseField::create([
                'title'   => 'Дата публикации',
                'storage' => 'publish_at',
                'type'    => 'Date',
            ]),
        ];
    }

    private static function about_fields() {
        return [

            'desc' => BaseField::create([
                'title' => 'Подробно',
                'storage' => 'desc',
                'type' => 'Wysiwyg'
            ]),

            'publish_at' => BaseField::create([
                'title'   => 'Дата публикации',
                'storage' => 'publish_at',
                'type'    => 'Date',
            ]),
        ];
    }


    private static function programm_fields() {
        return [

            'desc' => BaseField::create([
                'title' => 'Подробно',
                'storage' => 'desc',
                'type' => 'Wysiwyg'
            ]),

            'publish_at' => BaseField::create([
                'title'   => 'Дата публикации',
                'storage' => 'publish_at',
                'type'    => 'Date',
            ]),
        ];
    }



}
