<?php

namespace App\Lib\XFields\Category;

use App\Lib\XFields\Field\BaseField;
use App\Lib\XFields\Category\Base;

class SeoableCategory extends Base
{

    public static function hash() {

        return [
            'news' => 'contents.news',
        ];
    }


    public static function config()
    {
        $fields = static::category_fields();

        foreach( $fields as $alias => &$config)
        {
            $config = array_merge($config, static::seo_fields());
        }
        return $fields;
    }

    
    public static function category_fields() {
        return [];
    }


    private static function seo_fields() {

        $fields = [];
        foreach( static::seo_aliases_to_storages() as $alias => $storage)
        {
            $fields[$alias]= BaseField::create([
                'title'   => 'title',
                'storage' => $storage,
                'type'    => 'Text',
                'is_json' => true,
            ]);
        }

        return $fields;
    }


    protected static function seo_aliases_to_storages() {
        return [
            'seo_title' => 'seo_meta.title',
            'seo_keywords' => 'seo_meta.keywords',
            'seo_description' => 'seo_meta.description'
        ];
    }

}
