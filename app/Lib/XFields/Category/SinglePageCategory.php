<?php

namespace App\Lib\XFields\Category;

use App\Lib\XFields\Field\BaseField;
use App\Lib\XFields\Category\SeoableCategory;

class SinglePageCategory extends SeoableCategory
{

    public static function hash() {

        return [
            'about' => 'single_pages.about',
            'contacts' => 'single_pages.contacts',
            'cooperation' => 'single_pages.cooperation',
            'programm' => 'single_pages.programm',
        ];
    }

    public static function category_fields()
    {
        $fields = [];
        foreach (static::hash() as $key => $val)
        {
            $method = $key . "_fields";
            $fields[$key] = static::$method();
        }

        return $fields;
    }


    public static function about_fields()
    {
        return [

            'intro' => BaseField::create([
                'title' => 'Текст на главной',
                'storage' => 'props.intro',
                'type' => 'Text'
            ]),

            'desc' => BaseField::create([
                'title' => 'Основной текст',
                'storage' => 'desc',
                'type' => 'Wysiwyg'
            ]),

            'image_id' => BaseField::create([
                'title' => 'Картинка на главной',
                'storage' => 'props.image_id',
                'type' => 'Image'
            ]),

        ];
    }

    public static function contacts_fields()
    {

        return [

            'desc' => BaseField::create([
                'title' => 'Описание',
                'storage' => 'desc',
                'type' => 'Wysiwyg'
            ]),

//                'map_code' => BaseField::create([
//                    'title'   => 'Код карты',
//                    'storage' => 'props.map_code',
//                    'type'    => 'Text',
//                    'is_json' => true,
//                ]),


        ];
    }

    public static function cooperation_fields()
    {

        return [

            'desc' => BaseField::create([
                'title' => 'Описание',
                'storage' => 'desc',
                'type' => 'Wysiwyg'
            ]),


        ];
    }

    public static function programm_fields()
    {

        return [

            'desc' => BaseField::create([
                'title' => 'Описание',
                'storage' => 'desc',
                'type' => 'Wysiwyg'
            ]),


        ];
    }

}
