<?php

namespace App\Lib\Traits;

use Closure;
use InvalidArgumentException;

trait DynamicMethods
{
    private $methods = [];
    private $static_methods = [];

    // метод для динамического добавления методов ЭКЗЕМПЛЯРУ класса
    public function addMethod($methodName, $methodCallable)
    {
        if (!is_callable($methodCallable)) {
            throw new InvalidArgumentException('Second param must be callable');
        }
        $this->methods[$methodName] = Closure::bind($methodCallable, $this, get_class());
    }

    // метод для динамического добавления методов КЛАССУ
    public function addStaticMethod($staticMethodName, $methodCallable)
    {
        if (!is_callable($methodCallable)) {
            throw new InvalidArgumentException('Second param must be callable');
        }
        $this->static_methods[$staticMethodName] = Closure::bind($methodCallable, $this, get_class());

    }

    // переопределяем родительский метод
    public function __call($method, $parameters)
    {
        // если обратились к нашим XFields сеттерам или геттерам
        // запускаем их
        if (isset($this->methods[$method]))
        {
            return call_user_func_array($this->methods[$method], $parameters);
        }

        // иначе стандарная логика
        return parent::__call($method, $parameters);
    }


    public static function __callStatic($method, $parameters)
    {
        $instance = new static;

        if (isset($instance->static_methods[$method]))
        {
            $parameters['query'] = $instance->query();
            return call_user_func_array($instance->static_methods[$method], $parameters);
        }

        return parent::__callStatic($method, $parameters);
    }

}