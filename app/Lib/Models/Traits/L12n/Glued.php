<?php

namespace App\Lib\Models\Traits\L12n;
use Separated;

trait Glued
{
    use Separated;

    // TODO: Может стоит переписать не __construct, а метод new?
    public function __construct($attributes = [])
    {
        // устанавливаем значение по умолчанию, отработает только для нового объекта
        $this->attributes = array_add($this->attributes, 'localed_key', Uuid::generate());
    }

    public function to_locale($locale_id)
    {
        if ($this->locale_id == $locale_id) {
            return $this;
        }

        $localed = static::where([
            ['locale_id', $locale_id],
            ['localed_key', $this->localed_key],
        ])->first();


        return $localed;
    }



}