<?php

namespace App\Lib\Models\Traits\L12n;

trait Separated
{

    public function scopeByLocale($query, $locale_id) {
        return $query->where('locale_id', $locale_id);
    }

    public function scopeLocaled($query) {
        return $query->where('locale_id', \App::getLocale());
    }

}