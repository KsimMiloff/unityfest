<?php

namespace App\Lib\Models\Traits;

use DB;
use StdClass;

trait SeoableMixin
{

    public function __construct()
    {
        $this->casts['seo_meta'] = 'array';
    }


    public function save(array $options = [], $validate = true)
    {
        if ( empty( $this->slug ) )
        {
            $this->slug = $this->generate_slug();
        }
        return parent::save($options, $validate);
    }


    public function scopeBySlug($query, $slug) // bySlug
    {
        $possible_id = explode("-", $slug)[0];
        return $query->where('slug', $slug)
            ->orWhere('id', $slug)
            ->orWhere('id', $possible_id);
    }



    public function slug_placeholder()
    {
        if (! $this->exists ) { // если объекта еще нет в БД,
            return null; // то мы никак не можем сгенерить слаг
        }

        if ( $this->slug ) {
            return $this->slug;
        }

        return $this->generate_slug();
    }



    public function getSeoIdAttribute() //seo_id
    {
        if ($this->slug) {
            return $this->slug;
        }
        return $this->id . '-' . str_slug($this->slug_from_field());
    }



    public function setSlugAttribute($value)
    {
        $this->attributes['slug'] = str_slug($value);
    }



    public function seotags() {
        $seotags = new StdClass();

        $default_title = static::SLUG_FROM_FIELD;
        $default_title = $this->$default_title;

        $seotags->title = !empty( $this->seo_title ) ? $this->seo_title : $default_title;
        $seotags->keywords = $this->seo_keywords;
        $seotags->description = $this->seo_description;

        return $seotags;
    }



    private function slug_is_exists($slug) {
        $obj = DB::table($this->table)->where('slug', $slug)->first(); // ищем хотябы одну запись с таким же слагом

        if ($obj && $obj->id == $this->id ) // если объект существует и это текущий объект
        {
            return false; // для самого себя ну учитываем дубликаты
        }
        return (bool) $obj;
    }



    private function generate_slug($slug=null, $i=0) {

        $slug = $slug ?: str_slug($this->slug_from_field());

        if (empty($slug) ) {
            return null;
        }

        if ($this->slug_is_exists($slug) )
        {

            if ($i) // если мы уже добавляли $i в конец слага
            {
                $slug_chunks = explode("-", $slug); // разбиваем слаг на массив
                end($slug_chunks); // устанавливаем указатель на последний элемент, т.е. на наш прошлый $i
                $slug_chunks[key($slug_chunks)] = $i + 1; // обновляем последний эелемент, т.е. наращиваем $i
                $slug = implode("-", $slug_chunks); // собираем массив обратно в слаг
            } else {
                $slug = "{$slug}-1"; // добавляем единицу к слагу
            }

            $i++;

            return $this->generate_slug($slug, $i);
        } else {
            return $slug;
        }
    }



    private function slug_from_field() {
        $field = static::SLUG_FROM_FIELD;
        return $this->$field;
    }
}