<?php

namespace App\Models;

use App\Lib\Models\Traits\HasXFCategoryMixin;
use App\Lib\Models\Traits\SeoableMixin;
//use App\Lib\Models\Traits\L12n\Indivisible as L12n;
use App\Lib\XFields\Category\SinglePageCategory as Category;

use App\Lib\Models\BaseXFModel;


class SinglePage extends BaseXFModel
{
    const SLUG_FROM_FIELD = 'title';
    const CATEGORY_CLASS  = Category::class;


    use SeoableMixin {
        SeoableMixin::__construct as private __SeoableConstruct;
    }


    use HasXFCategoryMixin;

    protected $table = 'single_pages';

    protected $guarded = [];

    protected $casts = [
        'image_id'   => 'integer',
        'props'      => 'array',
    ];


    protected $attributes = [
        'is_visible' => true,
    ];


    protected $rules = array(
        'category_id' => 'required|in:about,cooperation,contacts,programm',
//        'desc'        => 'required',
    );


    public function __construct($attributes = [])
    {
//        print_r($attributes);exit;

        $this->__SeoableConstruct();
        parent::__construct($attributes);
    }


    public function scopeVisible($query) {
        return $query->where('is_visible', true);
    }


    public function scopeForMainPage($query) {
        return $query->where('is_visible', true);
    }


    public function xfields()
    {
        return $this->category->fields();
    }

}
