<?php

namespace App\Models;

use App\Lib\Models\Traits\LifeCycle;
use App\Lib\Models\Traits\HasXFCategoryMixin;
use App\Lib\Models\Traits\SeoableMixin;
//use App\Lib\Models\Traits\L12n\Separated as L12n;
use App\Lib\XFields\Category\ContentCategory as Category;

use App\Lib\Models\BaseXFModel;


class Content extends BaseXFModel
{
    const SLUG_FROM_FIELD = 'title';
    const CATEGORY_CLASS  = Category::class;

    use LifeCycle {
        LifeCycle::__construct as private __LifeCycleConstruct;
    }

    use SeoableMixin {
        SeoableMixin::__construct as private __SeoableConstruct;
    }

    use HasXFCategoryMixin;
//        L12n;

    protected $table = 'contents';

    protected $guarded = [];

    protected $casts = [
        'image_id'   => 'integer',
        'props'      => 'array',
    ];


    protected $attributes = [
        'is_visible' => true,
    ];


    protected $rules = array(
        'title'       => 'required',
        'category_id' => 'required|in:news,gallery,about,programm',
//        'desc'        => 'required',
    );



    public function scopeVisible($query) {
        $today = \Carbon\Carbon::today();
        return $query
            ->where('is_visible', true)
            ->where('publish_at', '<=', $today);
    }


    public function scopeForMainPage($query) {
        return $query->where('is_visible', true);
    }


    public function scopeFresh($query) {
        return $query->whereNotNull('publish_at')
            ->orderBy('publish_at', 'desc')
            ->orderBy('created_at', 'desc');
    }


    public function scopeUltraDate($query, $type, $date_field = 'created_at') {
        $obj = $query->whereNotNull($date_field)
            ->select($date_field)
            ->orderBy($date_field, ($type == 'min' ? 'desc' : 'asc'))
            ->first();

        if ($obj) {
            return \Carbon\Carbon::parse($obj->$date_field);
        }

        return null;
    }


    public function __construct($attributes = [])
    {
        $this->__LifeCycleConstruct();
        $this->__SeoableConstruct();
        parent::__construct($attributes);
    }


    public function xfields()
    {
        return $this->category->fields();
    }

}
