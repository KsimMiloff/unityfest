@extends('layouts.site')

@include('partials._seotags', ['seotags' => $page->seotags()])


@section('left_col')
    {!! LastNewsCell::show() !!}
@stop

@section('content')

    <div class="text">

        {!! $page->intro !!}

    </div>

    <div class="gallery-teaser">
        <h2>
            <a href="{!! route('galleries.index') !!}">Галерея</a>
        </h2>

        <div class="teaser">
            {!! Html::picture($page->image_id, ['size' => '600x382']) !!}
        </div>
    </div>
@stop