@extends('layouts.site')

@include('partials._seotags', ['seotags' => $page->seotags()])

@section('left_col')

@stop

@section('content')

    <div class="text">

        <article id="post-5" class="post-5 page type-page status-publish hentry">

            <h1 class="entry-title">{!! $page->title !!}</h1>
            <div class="entry-content">
                {!! $page->desc !!}
            </div>


        </article>
    </div>

@stop

