<?php $options = collect($categories)->map( function($category, $key) { return $category->title('plural'); }) ; ?>

{!! Form::select( 'category_id', $options, request()->category_id, ['placeholder' => 'Категории...', 'class' => "form-control"] ) !!}