<div class="row">
    <nav class="navbar navbar-default">
        <div class="container-fluid">
            <div class="navbar-header">
                <span class="navbar-brand">Фильтровать по...</span>
            </div>

            <div class="collapse navbar-collapse navbar-form navbar-left pull-right" id="bs-example-navbar-collapse-2">
                <form class="form-inline">
                    {!! $filters !!}

                    <div class="form-group">

                    </div>

                    <button type="submit" class="btn btn-primary glyphicon glyphicon-filter"></button>

                </form>
            </div>

        </div>
    </nav>
</div>