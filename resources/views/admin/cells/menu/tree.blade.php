<ul class="{!! isset( $class ) ? $class : 'dropdown-menu' !!}">

    @if ( count( $node->branches ) )
        @foreach($node->branches as $subnode)

            {!! AdminMenuCell::menu_item( $subnode ) !!}

        @endforeach

    @endif

</ul>
