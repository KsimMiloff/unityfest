<nav class="navbar navbar-default">
    <div class="container">

        <div class="container-fluid">
            <div class="navbar-header">
                <a class="navbar-brand" href="{{ url('/') }}"> Unity Fest </a>
            </div>

            <div class="collapse navbar-collapse" id="navbar-menu">
                @if ( $root_node->find_by_chain('left') )
                    @include('admin.cells.menu.tree', ['node' => $root_node->find_by_chain('left'), 'class' => 'nav navbar-nav'])
                @endif

                @if ($root_node->find_by_chain('right'))
                    @include('admin.cells.menu.tree', ['node' => $root_node->find_by_chain('right'), 'class' => 'nav navbar-nav navbar-right'])
                @endif
            </div>

        </div>
    </div>
</nav>