<?php

    $li_class = [];
    $a_conf = [];

    if ( count( $node->branches ) )
    {
        $li_class[]= $node->level() > 2 ? 'dropdown-submenu' : 'dropdown';
        //$li_class[]= 'dropdown-submenu';

        $a_conf[]= 'class="dropdown-toggle" data-toggle="dropdown"';
    }

    $li_class = implode(' ', $li_class);
    $a_conf = implode(' ', $a_conf);

?>

<li class="{!! $li_class !!}">

    @if ($title)
        <a href="{!! ($url ? $url : '#' ) !!}" {!! $a_conf !!}>
            {!! $title !!}

            @if (count( $node->branches ) && $node->level() == 2)
                <span class="caret"></span>
            @endif
        </a>
    @endif

    @if (count( $node->branches ))
        @include('admin.cells.menu.tree', ['node' => $node])
    @endif

</li>