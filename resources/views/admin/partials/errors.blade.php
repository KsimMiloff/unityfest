@if (count($errors) > 0)
    <div class="alert alert-warning">
        Что-то <strong>не так</strong>
        с данными в вашей форме!<br><br>
        <ul>
            @foreach ($errors as $error)
                <li>{{ $error }}</li>
            @endforeach
        </ul>
    </div>
@endif