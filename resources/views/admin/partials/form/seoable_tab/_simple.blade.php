<div id="seo" class="tab-pane fade in">

    <div class="row top-buffer">

        <div class="form-group">
            {!! Form::label("{$seoble_type}[slug]", 'ЧПУ', ['class' => 'control-label col-sm-2'])  !!}
            <div class="col-sm-8">
                {!! Form::text("{$seoble_type}[slug]", $seoable->slug_placeholder(), array('class' => 'form-control')) !!}
                <small>
                    символьный идентификатор ресурса (только латиница)
                </small>
            </div>
        </div>

        <div class="form-group">
            {!! Form::label("{$seoble_type}[seo_title]", 'title', ['class' => 'control-label col-sm-2'])  !!}
            <div class="col-sm-8">
                {!! Form::text("{$seoble_type}[seo_title]", $seoable->seo_title, array('class' => 'form-control')) !!}
                <small>
                    заголовок документа, если пустой, то берется название объекта
                </small>
            </div>
        </div>

        <div class="form-group">
            {!! Form::label("{$seoble_type}[seo_keywords]", 'keywords', ['class' => 'control-label col-sm-2'])  !!}
            <div class="col-sm-8">
                {!! Form::text("{$seoble_type}[seo_keywords]", $seoable->seo_keywords, array('class' => 'form-control')) !!}
                <small>
                    список слов и словосочетаний через запятую
                </small>
            </div>
        </div>

        <div class="form-group">
            {!! Form::label("{$seoble_type}[seo_description]", 'description', ['class' => 'control-label col-sm-2'])  !!}
            <div class="col-sm-8">
                {!! Form::textarea("{$seoble_type}[seo_description]", $seoable->seo_description, array('class' => 'form-control', 'rows' => 3)) !!}
            </div>
        </div>

    </div>

</div>