<div id="seo" class="tab-pane fade in">

    <div class="row top-buffer">

        <div class="form-group">
            {!! Form::label("{$seoble_type}[slug]", 'ЧПУ', ['class' => 'control-label col-sm-2'])  !!}
            <div class="col-sm-8">
                {!! Form::text("{$seoble_type}[slug]", $seoable->slug_placeholder(), array('class' => 'form-control')) !!}
                <small>
                    символьный идентификатор ресурса (только латиница)
                </small>
            </div>
        </div>


        <div class="form-group">
            <div class="col-sm-8 col-sm-offset-2">
                <ul class="nav nav-pills">
                    @foreach(App\Lib\Locale::$list as $lang => $title)
                        <li class="{!! $lang == 'ru' ? 'active' : '' !!}"><a data-toggle="tab" href="#seo-{!! $lang !!}">{!! $lang !!}</a></li>
                    @endforeach
                </ul>
            </div>
        </div>

        <div class="tab-content">

            @foreach(['ru', 'en', 'kz'] as $lang)

                <div id="seo-{!! $lang !!}" class="tab-pane fade in {!! $lang == 'ru' ? 'active' : '' !!}">



                    <div class="form-group">
                        {!! Form::label("{$seoble_type}[seo_title_{$lang}]]", "title ({$lang})", ['class' => 'control-label col-sm-2'])  !!}
                        <div class="col-sm-8">
                            {!! Form::text("{$seoble_type}[seo_title_{$lang}]", $seoable->seo_title($lang), array('class' => 'form-control')) !!}
                            <small>
                                заголовок документа, если пустой, то берется название объекта
                            </small>
                        </div>
                    </div>

                    <div class="form-group">
                        {!! Form::label("{$seoble_type}[seo_keywords_{$lang}]", "keywords ({$lang})", ['class' => 'control-label col-sm-2'])  !!}
                        <div class="col-sm-8">
                            {!! Form::text("{$seoble_type}[seo_keywords_{$lang}]]", $seoable->seo_keywords($lang), array('class' => 'form-control')) !!}
                            <small>
                                список слов и словосочетаний через запятую
                            </small>
                        </div>
                    </div>

                    <div class="form-group">
                        {!! Form::label("{$seoble_type}[seo_description_{$lang}]", "description ({$lang})", ['class' => 'control-label col-sm-2'])  !!}
                        <div class="col-sm-8">
                            {!! Form::textarea("{$seoble_type}[seo_description_{$lang}]]", $seoable->seo_description($lang), array('class' => 'form-control', 'rows' => 3)) !!}
                        </div>
                    </div>
                </div>

            @endforeach

        </div>
    </div>

</div>