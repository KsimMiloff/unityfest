
<div class="form-group">
    <div class="row top-buffer">
        <div class="col-sm-8 col-md-offset-2">
            {!! Form::submit('Сохранить', ['name' => 'action[save]', 'class' => 'btn btn-primary']) !!}

            @unless ($object->exists)
                {{--<span> ... и ... </span>--}}
{{--                {!! Form::submit('добавить следующий', ['name' => 'action[save_and_new]', 'class' => 'btn btn-primary']) !!}--}}
            @endunless

            <div class="pull-right">

                {!! link_to(URL::previous(), 'Отмена', ['class' => 'btn btn-primary']) !!}

                @if (isset($lifecycled) && $lifecycled)
                    @if ($object->exists)

                        @if ($object->is_live)
                            {!! Form::submit('В архив', ['name' => 'action[move_to_archive]', 'class' => 'btn btn-warning']) !!}
                        @elseif ($object->is_archive)
                            {!! Form::submit('Восстановить', ['name' => 'action[move_to_live]', 'class' => 'btn btn-success']) !!}
                        @endif

                    @endif
                @endif

            </div>

        </div>
    </div>
</div>