<div id="info" class="tab-pane fade in active">

    <div class="row top-buffer">

        <div class="form-group">
            {!! Form::label('single_page[title]', 'Заголовок', ['class' => 'control-label col-sm-2'])  !!}
            <div class="col-sm-8">
                {!! Form::text('single_page[title]', $page->title, array('class' => 'form-control')) !!}
            </div>
        </div>


        @foreach($page->category->fields() as $alias => $field)
            @if (! starts_with($alias, 'seo_'))
                {!! $field->widget(['model' => 'single_page', 'alias' => $alias, 'value' => $page->$alias]) !!}
            @endif
        @endforeach

        <div class="form-group">
            <div class="col-sm-3 col-sm-offset-2">
                <div class="checkbox">
                    <label>
                        {!! Form::boolean('single_page[is_visible]', $page->is_visible) !!} Показывать на сайте
                    </label>
                </div>
            </div>
        </div>

    </div>
    
</div>