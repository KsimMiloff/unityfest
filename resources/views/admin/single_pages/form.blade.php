@extends('layouts.admin')

@section('content')

    <div class="row">
        <?php

        $single_pages = [
                'about' => 'about',
                'programm' => 'programm',
        ];

        ?>

        @if ( array_has($single_pages, $page->category_id))
            {{--{!! link_to_route('admin.single_pages', $page->category->title(), ['category_id' => $single_pages[$page->category_id]] ) !!}--}}
                {!! AdminContentCrumbs::show( ['content' => $page ] ) !!}
        @endif
    </div>

    <h1>
        {!! $page->category->title() !!}
    </h1>


    <div class="row">

        <ul class="nav nav-tabs">
            <li class="active"><a data-toggle="tab" href="#info">Инфо</a></li>
            <li class=""><a data-toggle="tab" href="#seo">SEO</a></li>
        </ul>


        {!! Form::resource($page, ['route' => 'admin.single_pages', 'class' => 'form-horizontal']) !!}

        {!! Form::hidden('single_page[category_id]', $page->category_id) !!}

        <div class="tab-content">

            @include('admin.partials.errors', ['errors' => $page->errors])

            @include("admin.single_pages.form._info_tab")
            @include("admin.partials.form.seoable_tab._simple", ['seoable' => $page, 'seoble_type' => 'single_page'])

        </div>

        @include("admin.partials.form._button_bar", ['object' => $page])


        {!! Form::close() !!}

    </div>

@stop

