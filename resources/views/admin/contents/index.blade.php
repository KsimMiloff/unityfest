@extends('layouts.admin')

@section('content_bar')

    {!! AdminFilterCell::show([
        //'category' => ['categories' => $categories],
        'lifecycle' => ['route' => 'admin.contents.index']
        ])
    !!}

@stop

@section('content')


    <div class="row">
        <?php

            $single_pages = [
                'about' => 'about',
                'programm' => 'programm',
            ];

        ?>

        @if ( array_has($single_pages, $category->id))
            {!! link_to_route('admin.single_pages', $category->title(), ['category_id' => $single_pages[$category->id]] ) !!}
        @endif
    </div>

    <div class="row">


        <h1>{!! $category->title('plural') !!}

            <div class="btn-group">
                {!! link_to_route('admin.contents.create', 'Добавить', ['category_id' => $category->id], ['class' => 'btn btn-primary']) !!}
            </div>

        </h1>

        @if ($contents->isEmpty())
            <p>Нет ни одного контента</p>

        @else

            <table class="table table-hover">
                <colgroup>
                    <col width="70%"/>
                </colgroup>
                <thead>
                    <tr>
                        <th>Заголовок</th>
                        <th>Тип</th>
                    </tr>
                </thead>
                <tbody>

                    @foreach ($contents as $content)
                        <tr>

                            <td> {!! link_to_route('admin.contents.edit', $content->title, ['content_id' => $content->id]) !!} </td>
                            <td> {!! $content->category->title() !!} </td>

                        </tr>

                    @endforeach

                </tbody>
            </table>

        @endif

    </div>
@stop