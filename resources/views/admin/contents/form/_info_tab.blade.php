<div id="info" class="tab-pane fade in active">

    <div class="row top-buffer">

        <div class="form-group">
            {!! Form::label('content[title]', 'Заголовок', ['class' => 'control-label col-sm-2'])  !!}
            <div class="col-sm-8">
                {!! Form::text('content[title]', $content->title, array('class' => 'form-control')) !!}
            </div>
        </div>


        @foreach($content->category->fields() as $alias => $field)
            @if (! starts_with($alias, 'seo_'))
                {!! $field->widget(['model' => 'content', 'alias' => $alias, 'value' => $content->$alias]) !!}
            @endif
        @endforeach

        <div class="form-group">
            <div class="col-sm-3 col-sm-offset-2">
                <div class="checkbox">
                    <label>
                        {!! Form::boolean('content[is_visible]', $content->is_visible) !!} Показывать на сайте
                    </label>
                </div>
            </div>
        </div>

    </div>
</div>