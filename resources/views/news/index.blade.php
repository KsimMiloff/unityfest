@extends('layouts.site')

@include('partials._seotags', ['seotags' => (object)[
    'title' => 'Новости',
    'keywords' => '',
    'description' => ''
]])

@section('left_col')
    @include('cells.left_col.menu', ['cell_name' => 'news_menu', 'route' => 'news.index'])
@stop

@section('content')

    <h1>Новости</h1>

    <div class="text">

        @foreach($news as $n)

        <div class="date">
            {!! \Carbon\Carbon::parse($n->publish_at)->format('d.m.Y') !!}
        </div>

        <div class="post">

            <h2 class="entry-title news-h2">
                <a href="{!! route('news.show', ['id' => $n->seo_id]) !!}">
                {!! $n->title !!}
                </a>
            </h2>

            <div class="entry-content">
                <p>
                    {!! $n->announce !!}
                </p>

                {!! $n->desc !!}
            </div>

        </div>

        @endforeach
    </div>

@stop

