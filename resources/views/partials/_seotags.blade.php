@section('seotags')
    <title>{!! $seotags->title !!}</title>
    <meta name="keywords" content="{!! $seotags->keywords !!}">
    <meta name="description" content="{!! $seotags->description !!}">
@stop