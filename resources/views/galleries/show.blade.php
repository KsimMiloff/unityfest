@extends('layouts.site')

@include('partials._seotags', ['seotags' => $gallery->seotags()])

@section('left_col')
    @include('cells.left_col.menu', ['cell_name' => 'gallery_menu', 'route' => 'galleries.show'])
@stop

@section('content')

    <h1>{!! $gallery->title !!}</h1>

    <div class="text">

        @if ( count( $gallery->image_ids ) )
            <div class="fotorama"  data-width="1000px" data-autoplay="true">
                @foreach( $gallery->image_ids as $image_id)
                    {!! Html::picture($image_id, ['size' => '800x']) !!}
                @endforeach
            </div>
        @endif

    </div>

@stop

