<div class="menu-top_menu-container">
    <ul id="menu-top_menu" class="left-menu dashed ">



        @foreach($contents as $content)
            <li>
                <a href="{!! route($route, array_merge($params, ['id' => $content->seo_id])) !!}">{!! $content->title !!}</a>
            </li>
        @endforeach
    </ul>
</div>