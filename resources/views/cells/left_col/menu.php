<?php

use App\Models\Content;
use App\Lib\XFields\Category\ContentCategory;

if (! isset($params)) {
    $params = [];
}

function contents($category_id, $route, $params)
{
    $contents = Content::live()->visible()
        ->byCategoryId($category_id)
        ->fresh()
        ->get();

    return view('cells.left_col.contents_menu', [
        'contents' => $contents,
        'route' => $route,
        'params' => $params
    ]);
}


function about_menu($route, $params) {
    return contents('about', $route, $params);
}


function programm_menu($route, $params) {
    return contents('programm', $route, $params);
}


function gallery_menu($route, $params) {
    return contents('gallery', $route, $params);
}


function news_menu($route, $params) {
    $years = Content::live()->visible()
        ->byCategoryId('news')
        ->select('publish_at')
        ->fresh()
        ->get()
        ->map( function( $date) {
            $year = \Carbon\Carbon::parse($date['publish_at'])->year;
            return (object) [
                'year' => $year
            ];
        } )->toArray();

    $years = collect($years)
        ->unique();

    return view('cells.left_col.news_menu', [
        'years' => $years,
        'route' => $route,
        'params' => $params
    ]);
}



echo $cell_name($route, $params);