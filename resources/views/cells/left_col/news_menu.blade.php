<div class="menu-top_menu-container">
    <ul id="menu-top_menu" class="left-menu dashed ">



        @foreach($years as $year)
            <li>
                <a href="{!! route($route, array_merge($params, ['year' => $year->year])) !!}">{!! $year->year !!}</a>
            </li>
        @endforeach
    </ul>
</div>