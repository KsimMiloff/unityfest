<nav class="top-menu">
    @foreach($menu_items as $item)
        <li><a href="{!! $item['url'] !!}" class="black {!! $item['active'] ? 'active' : '' !!}">{!! $item['title'] !!}</a></li>
    @endforeach
</nav>
