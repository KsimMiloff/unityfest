<h2>Новости</h2>

@foreach( $news as $n )
    <div class="news-item">
        <span class="date">
            {!! \Carbon\Carbon::parse($n->publish_at)->format('d.m.Y') !!}
        </span>
        <span class="news-title">
            <a href="{!! route('news.show', ['id' => $n->seo_id]) !!}">
                {!! $n->title !!}
            </a>
        </span>
        <span class="news-intro">
            {!! $n->announce !!}
        </span>
    </div>
@endforeach


