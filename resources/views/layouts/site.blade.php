<!DOCTYPE html>
<html lang="ru">
    <head>
        <meta charset="utf-8">

        @yield('seotags')

        <link rel="icon" type="image/png" href="{{ URL::asset('assets/images/favicon.ico') }}" />

        <link href="{{ asset('/assets/styles/site/reset.css') }}" rel="stylesheet">
        <link href="{{ asset('/vendor/styles/fotorama.css') }}" rel="stylesheet">
        <link href="{{ asset('/assets/styles/site/skeleton.css') }}" rel="stylesheet">
        <link href="{{ asset('/assets/styles/site/styles.css') }}" rel="stylesheet">


        <?php
            $ccs_array = Array('blue2purple', 'yellow2green', 'purple2orange', 'red2azure');
            $rand_css  = $ccs_array[array_rand($ccs_array)];
        ?>

        <link href="{{ asset("/assets/styles/site/$rand_css.css") }}" rel="stylesheet">

        <script src="{{ asset('/vendor/scripts/jquery.js') }}"></script>
        <script src="{{ asset('/vendor/scripts/fotorama.js') }}"></script>


        @yield('styles')

        @yield('scripts')
    </head>
    <body>

    <div class="wrapper">
        <div class="stretcher">
            <header>
                <div class="gradient top">
                    {!! MenuCell::show() !!}

                    <div class="logo">
                        <a href="/"><img src="{{ URL::asset('assets/images/logo.gif') }}"></a>
                    </div>
                </div>

            </header>


            <div class="content">
                <div class="column left-column">
                    @yield('left_col')

                    <div class="baners">
                        <div class="banner-item">
                            <img src="{{ URL::asset('assets/images/tmp/banner.jpg') }}">
                        </div>
                    </div>


                </div>

                <div class="column right-column">
                    @yield('content')
                </div>
            </div>


            <div class="sponsors">
                <img src="{{ URL::asset('assets/images/imgo.jpg') }}">
                <div style="clear: both"></div>
            </div>

        </div>

        <footer>
            <div class="gradient bottom">
                <span class="copyright">© 2013–{!! \Carbon\Carbon::today()->format('Y') !!} Unity Femily Fest</span>
            </div>
        </footer>
    </div>
    </body>
</html>