@extends('layouts.site')

@include('partials._seotags', ['seotags' => $content->seotags()])

@section('left_col')
    @include('cells.left_col.menu', ['cell_name' => $content->category_id . '_menu', 'route' => 'contents.show'])
@stop

@section('content')

    <div class="text">

        <article id="post-5" class="post-5 page type-page status-publish hentry">

            <h1 class="entry-title">{!! $content->title !!}</h1>
            <div class="entry-content">
                {!! $content->desc !!}
            </div>


        </article>
    </div>

@stop

