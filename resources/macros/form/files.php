<?php


Form::macro('files', function($name, $value, $options=[]) {
    $id       = array_get($options, 'id') ?: str_replace(['[', ']'], '_', $name);
    $class    = array_get($options, 'class', '');
    $multiple = array_get($options, 'multiple') == true;

    if ($multiple) {
        $class = "{$class} multiple";
    }

    $options  = array_except($options, ['class', 'styles', 'multiple']);

    $token = csrf_token();
    $url   = route('files.store');

    $file_field_name   = $name . '_file_field';
    $file_field_id     = $id . '_file_field';
    $file_button_class = '';

    array_set($options, 'id', $file_field_id);
    array_set($options, 'data-behavior', 'file_dialog');
    array_set($options, 'data-files', '[]');

    if ($multiple) {
        array_set($options, 'multiple', true);
        $name = "{$name}[]";
    } else if ($value) {
        $value = [$value];
        $file_button_class = "{$file_button_class} hide";
    }

    $fields = collect($value)->map(function ($v) use ($name)  {
        $field = Form::hidden($name, $v);
        $src   = Html::url_to_picture($v, ['size' => '100']);

        $thumb = "
            <div class='form-thumb'>
                <i class='glyphicon glyphicon-remove form-thumb-remove' data-behavior='cancel-file'></i>
                <div class='form-thumb-cell'>
                    <img src='$src'>
                </div>
                $field
            </div>
        ";

        return $thumb;
    })->implode('');

    $default_field = Form::hidden($name, "");
    $file_field    = Form::file($file_field_name, $options);

    $html = "
        <div id='{$id}' class='{$class} form-files-area form-file-field' data-behavior='file_ids_container' data-field_name='{$name}' data-token='{$token}' data-url='{$url}' data-multiple='{$multiple}'>
            $file_field
            <div class='file-button form-thumb {$file_button_class}' data-behavior='file_dialog_btn'>
                <div class='form-thumb-cell'>
                    <i class='glyphicon glyphicon-upload'></i>
                </div>
                $default_field
            </div>
            $fields
        </div>
        <script>
            $(function() {
                $('#{$file_field_id}').data('container', $('#{$id}'));
                $('#{$file_field_id}').data('button', $('#{$id}').find('[data-behavior=file_dialog_btn]'));

            });
        </script>
    ";
    return $html;

});


Form::macro('images', function($name, $value=null, $options=[]) {

    array_set($options, 'accept', 'image/*');

    return Form::files($name, $value, $options);

});
