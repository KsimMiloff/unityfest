<?php

return [
    'store.success' => 'Запись успешно создана',
    'store.error' => 'Создать запись не удалось',

    'update.success' => 'Запись успешно изменена',
    'update.error' => 'Запись изменить не удалось',

    'move_to_archive.success' => 'Запись перемещена в архив',
    'move_to_live.success' => 'Запись востановлена из архива',


    'list.user' => 'Список пользователей',
    'new.user' => 'Новый пользователь',
    'edit.user' => ':title',

    'new.content' => 'Новый контент',
    'edit.content' => 'Редактирование контента &laquo;:title&raquo;',

    'list.good' => 'Список товаров',
    'new.good'  => 'Новый товар',
    'edit.good' => 'Редактирование товара &laquo;:title&raquo;'

];