<?php

return [
    'model' => 'Контент',

    'news' => 'Новости',
    'news.singular' => 'Новость',
    'news.plural'   => 'Новости',

    'gallery' => 'Галерея',
    'gallery.singular' => 'Галерея',
    'gallery.plural'   => 'Галереи',

    'about' => 'О фестивале',
    'about.singular' => 'страница "О фестивале"',
    'about.plural'   => 'страницы "О фестивале"',

    'programm' => 'Программа',
    'programm.singular' => 'страница "Программы"',
    'programm.plural'   => 'страницы "Программы"',


];