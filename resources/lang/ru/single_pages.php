<?php

return [
    'about.singular' => 'О фестивале',
    'contacts.singular' => 'Контактные данные',
    'cooperation.singular' => 'Сотрудничество',
    'programm.singular' => 'Программа',
];