$(function(){

    section_list_id = 'section_list';
    $form_dialog  = $('#content-form-modal');
    $section_list = $('#' + section_list_id);

    $form_dialog.submit(function(e)
    {
        e.preventDefault();

        $form  = $(this).find('form');
        url    = $form.attr('action');
        token  = $form.find('[name=_token]').val();
        method = $form.find('[name=_method]').val();
        type   = $form.attr('method');

        xhr = $.ajax(
            {type: type, url: url, data: $form.serialize()
        });

        xhr
            .done(function(html) {
                response_el_id = $(html).data('id');

                $section = $section_list.find( '[data-id=' + response_el_id + ']' );

                if ( $section.length )
                {
                    $section.replaceWith( html );
                } else {
                    $section_list.append(html);
                }
            })
            .fail(function() {
                alert('Сохранить секцию не удалось');
            })
            .always(function() {
                $form_dialog.modal('hide');
            });

    });

    $(document).on('click', '[data-behavior=add_section_content], [data-behavior=edit_section_content]', function() {
        url = $(this).data('url');

        xhr = $.ajax({ url: url, cache: false});
        xhr
            .done(function(html) {
                $form_dialog.find('.modal-content').html(html);
                $form_dialog.modal('show');
            })
            .fail(function() {

            })
            .always(function() {

            });
    });


    $(document).on('click', '[data-behavior=delete_section_content]', function() {
        url = $(this).data('url');

        $section = $(this).closest('.list-group-item');

        xhr = $.ajax({ url: url, cache: false, type: 'post', data: { _method: 'DELETE' }, dataType: "json" });
        xhr
            .done(function(response) {
                $section.remove();
            })
            .fail(function() {

            })
            .always(function() {

            });
    });


    if ($form_dialog.length) {
        var section_list = document.getElementById(section_list_id);
        var sortable = Sortable.create(section_list, {
            handle: ".handler",

            onEnd: function (e) {
                order = [];
                $('.list-group-item').each(function( i, list_item ) {
                    $list_item = $(list_item);

                    order.push( $list_item.data( 'id' ));
                });

                url = $section_list.data('orderize-url')
                xhr = $.ajax({ type: "Post", url: url, data: { order: order }, dataType: "json"});
                xhr.done( function( response ) {

                });


                xhr.fail( function( response ) {
                    alert( 'Изменить порядок не удалось' );
                });


            }
        });
    }
});