<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSinglePagesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('single_pages', function (Blueprint $table) {
            $table->increments('id');
            $table->string('slug')->nullable();
            $table->string('category_id');

            $table->string('title');
            $table->text('desc');

            $table->string('state');
            $table->text('props')->nullable();
            $table->boolean('is_visible')->nullable();

            $table->text('seo_meta');

            $table->timestamps();

        });

        Schema::table('single_pages', function (Blueprint $table) {
            $indexes = ['id', 'slug', 'category_id', 'state', 'is_visible'];
            foreach ( $indexes as $index) $table->index($index);
        });

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('single_pages');
    }
}
