<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;
use Webpatser\Uuid\Uuid;


class CreateContentsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('contents', function (Blueprint $table) {
            $table->increments('id');
            $table->string('slug')->nullable();

            $table->string('title');
            $table->string('category_id');
            $table->string('state');
            $table->text('desc')->nullable();
            $table->text('props')->nullable();
            $table->boolean('is_visible')->nullable();
            $table->integer('position')->nullable();

            $table->string('locale_id');
            $table->text('seo_meta');

            $table->date('publish_at');


            $table->timestamps();

        });


        Schema::table('contents', function (Blueprint $table) {
            $indexes = ['id', 'slug', 'category_id', 'state', 'is_visible', 'locale_id'];
            foreach ( $indexes as $index) $table->index($index);
        });

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('contents');
    }
}
